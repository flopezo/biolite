/* BioLite - Tools for processing gene sequence data and automating workflows
 * Copyright (c) 2012-2014 Brown University. All rights reserved.
 * 
 * This file is part of BioLite.
 * 
 * BioLite is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * BioLite is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with BioLite.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
#include <math.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <string>
#include <sstream>
#include "seqio.hpp"

#define PROGNAME "pileup_stats"
#include "util.h"

using namespace std;

void print_usage()
{
	cout << "\n"
"usage: "PROGNAME" -i PILEUP -o HIST -m OVERLAP\n"
"\n"
"Reads a SAMtools pileup file and uses it to find potential sequence disconnects.\n"
"A histogram of all disconnect events encountered is written to the HIST file.\n"
"\n"
"  -i  specify the PILEUP input file\n"
"  -o  specify the HIST output file\n"
"  -m  specify the min OVERLAP (default: 0)\n"
"\n";
}

void print_stats(
		const char* input_name,
		const char* hist_name,
		int min_overlap
		)
{
	istream *in;
	if (!strcmp(input_name, "-"))
		in = &cin;
	else
		in = new ifstream(input_name);

	ostream *hist = NULL;

	vector<string> names;
	vector<int> disconnects;

	if (hist_name != NULL)
	{
		hist = new ofstream(hist_name);
	}
	else
	{
		hist = &cout;
	}

	long total = 0;
	long bases = 0;
	long transcripts = 0;

	string mapline_str;
	string reference;
	const char *mapline;
	while (in->good())
	{
		getline(*in, mapline_str);
		mapline = mapline_str.c_str();
		bases++;

		const char *tab = strtok((char *)mapline, "\t");
		for (int idx = 0; tab != NULL; idx++)
		{
			int mapped_reads;
			int ends = 0;
			switch (idx)
			{
				case 0: // reference name
					if (reference != string(tab))
					{
						transcripts++;
						if (names.size() > 0)
						{
							// two are the beginning and end of the transcript (real)
							disconnects.back() -= 2;
							total-=2;
							*hist << names.back() << "\t" << disconnects.back() << endl;
						}
						reference = string(tab);
						names.push_back(reference);
						disconnects.push_back(0);
					}
					break;
				case 1: // base number
					break;
				case 3: // mapped reads
					mapped_reads = atoi(tab);
					break;
				case 4: // map locations
					for (int i = 0; tab[i] != '\0'; i++)
						if (tab[i] == '$' || tab[i] == '^')
							ends++;
					if (ends >= mapped_reads - min_overlap)
					{
						disconnects.back()++;
						total++;
					}
					break;
				default:
					break;
			}
			tab = strtok(NULL, "\t");
		}
	}

	DIAGNOSTICS("total_disconnects", total, "")
	DIAGNOSTICS("total_bases", bases, "")
	DIAGNOSTICS("total_transcripts", transcripts, "")

	if (hist != NULL) {
		DIAGNOSTICS("hist_file", hist_name, "")
		delete hist;
	}

	if (in != &cin)
		delete in;
}

int main(int argc, char** argv)
{
	const char* input_name = NULL;
	const char* hist_name = NULL;

	int min_overlap = 0;

	int c;
	while ((c = getopt(argc, argv, "vhi:o:m:")) != -1)
		switch (c) {
			case 'i':
				input_name = optarg;
				break;
			case 'o':
				hist_name = optarg;
				break;
			case 'm':
				min_overlap = atoi(optarg);
				break;
			case 'v':
				PRINT_VERSION
			case 'h':
			default:
				print_usage();
				exit(EXIT_SUCCESS);
		}

	/* Default to stdin. */
	if (!input_name) input_name = "-";

	print_stats(input_name, hist_name, min_overlap);

	return EXIT_SUCCESS;
}

