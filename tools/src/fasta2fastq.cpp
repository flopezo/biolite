/* BioLite - Tools for processing gene sequence data and automating workflows
 * Copyright (c) 2012-2014 Brown University. All rights reserved.
 * 
 * This file is part of BioLite.
 * 
 * BioLite is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * BioLite is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with BioLite.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <string.h>
#include <iostream>
#include <vector>
#include "seqio.hpp"

#define PROGNAME "fasta2fastq"
#include "util.h"

using namespace std;

void print_usage()
{
	cout << "\n"
"usage: "PROGNAME" -i FASTA [...] -q QUAL [...] [-o FASTQ] [-a] [-t OFFSET]\n"
"\n"
"Merges each FASTA file and its corresponding QUAL file into a FASTQ file\n"
"with the name <basename>.fastq, where <basename> in the FASTA name up to the\n"
"last period (or with name FASTQ if specified).\n"
"\n"
"FASTQ is *appended* to (not truncated).\n"
"\n"
"  -i  specify multiple FASTA input files\n"
"  -q  specify multiple (corresponding) QUAL input files\n"
"  -o  specify multiple (corresponding) FASTQ output files\n"
"  -a  scores in QUAL file are ASCII (default: numerical scores)\n"
"  -t  use OFFSET for converting ASCII quality scores (default: 33)\n"
"\n";
}

void fasta_to_fastq(
		const char* fasta_filename,
		const char* qual_filename,
		const char* fastq_filename,
		int offset,
		int qual_ascii
		)
{
	SeqIO fasta(fasta_filename);
	fasta.setQualityOffset(offset);
	fasta.setQualityInput(qual_filename, qual_ascii);
	fasta.setOutput(fastq_filename);

	while (fasta.nextRecordQual()) {
		fasta.printRecordFastq();
	}

	NOTIFY(fasta.countRecords() << " sequences in " << fasta_filename)
}

int main(int argc, char** argv)
{
	vector<const char*> fasta_names;
	vector<const char*> qual_names;
	vector<const char*> fastq_names;
	int offset = 33;
	int qual_ascii = 0;

	int c;
	while ((c = getopt(argc, argv, "vhi:q:o:at:")) != -1)
		switch (c) {
			case 'i':
				fasta_names.push_back(optarg);
				break;
			case 'q':
				qual_names.push_back(optarg);
				break;
			case 'o':
				fastq_names.push_back(optarg);
				break;
			case 't':
				offset = atoi(optarg);
				break;
			case 'a':
				qual_ascii = 1;
				break;
			case 'v':
				PRINT_VERSION
			case 'h':
			default:
				print_usage();
				exit(EXIT_SUCCESS);
		}

	/* If the number of FASTA and QUAL files don't match, there is a problem! */
	if (fasta_names.size() != qual_names.size())
		ARG_ERROR("mismatch between number of FASTA and QUAL files")

	if (fastq_names.size()) {
		if (fasta_names.size() != fastq_names.size())
			ARG_ERROR("mismatch between number of FASTA and FASTQ files")
	}

	if (fasta_names.size() == 0)
		ARG_ERROR("no FASTA files specified with -i")

	for (unsigned i=0; i<fasta_names.size(); i++) {
		const char* fastq_name;
		/* Get the basename. */
		string fasta_name(fasta_names[i]);
		string base_name(fasta_name, 0, fasta_name.find_last_of('.'));
		if (fastq_names.size()) {
			fastq_name = fastq_names[i];
		} else {
			base_name.append(".fastq");
			fastq_name = base_name.c_str();
		}
		NOTIFY("converting '" << fasta_names[i] << "'")
		fasta_to_fastq(
				fasta_names[i], qual_names[i], fastq_name,
				offset, qual_ascii);
	}

	return EXIT_SUCCESS;
}

