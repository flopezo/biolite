/* BioLite - Tools for processing gene sequence data and automating workflows
 * Copyright (c) 2012-2014 Brown University. All rights reserved.
 * 
 * This file is part of BioLite.
 * 
 * BioLite is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * BioLite is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with BioLite.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __BIOLITE_UTIL_H__
#define __BIOLITE_UTIL_H__

#include <getopt.h>
#include <string.h>
#include <stdlib.h>
#include <iostream>
#include "../config.h"

#define DIAGNOSTICS(attr, val, msg) \
	cerr << "[biolite] " << attr << "=" << val << " " << msg << endl; \
	cerr.flush();
#define NOTIFY(msg) cerr << PROGNAME": " << msg << endl; cerr.flush();
#define ERROR(msg) do{ NOTIFY(msg) exit(EXIT_FAILURE); }while(0);
#define PERROR(msg) do{ NOTIFY(msg) perror(PROGNAME); exit(EXIT_FAILURE); }while(0);
#define ARG_ERROR(msg) do{ NOTIFY(msg) print_usage(); exit(EXIT_FAILURE); }while(0);

/* crazy workaround to get __LINE__ as a string */
#define STRINGIFY(x) #x
#define STRINGIFY1(x) STRINGIFY(x)
#define AT __FILE__ ":" STRINGIFY1(__LINE__)

/* Check if an alloc failed. */
#define ALLOC_CHECK(buf) if (buf == NULL) { NOTIFY(AT << "allocation failed") }

#define PRINT_VERSION \
	printf("%s (%s) %s\n\nBuilt on %s (%s)\n%s\nCompiler: %s\nOptions: %s\n",\
		PROGNAME, PACKAGE_NAME, PACKAGE_VERSION,\
		PACKAGE_BUILD_HOST, PACKAGE_BUILD_HOST_OS, PACKAGE_BUILD_DATE,\
		PACKAGE_COMPILER, PACKAGE_COMPILE_OPTIONS);\
	printf("\nCopyright (c) 2012-2014 Brown University. All rights reserved.\n"\
"This is free software; see the source for copying conditions.  There is NO\n"\
"warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.\n"\
"\n");\
	exit(EXIT_SUCCESS);

#endif
