/* BioLite - Tools for processing gene sequence data and automating workflows
 * Copyright (c) 2012-2014 Brown University. All rights reserved.
 * 
 * This file is part of BioLite.
 * 
 * BioLite is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * BioLite is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with BioLite.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <vector>
#include <iostream>
#include "../seqio.hpp"

using namespace std;

int main(int argc, char** argv)
{
	vector<string> ids;
	vector<string> seqs;
	vector<string> quals;

	SeqIO fastq(argv[1]);
	while (fastq.nextRecord()) {
		ids.push_back(fastq.getIDLine());
		seqs.push_back(fastq.getSequence());
		quals.push_back(fastq.getQuality());
	}

	cout << ids.size() << endl;
	cout << seqs.size() << endl;
	cout << quals.size() << endl;

	return EXIT_SUCCESS;
}

