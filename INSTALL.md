# Installation

These are advanced instructions for installing directly from the tarball or for
installing a development version from the git repository. See the Quick Install
section of the README for instructions on how to install a release version
using pip, the Python package manager.


## Reference List of Dependencies

Python modules:

* biopython (1.60, 1.61)
* dendropy (3.12.0)
* docutils (0.9.1, 0.10)
* matplotlib (1.1.0, 1.1.1rc, 1.1.1)
* networkx (1.6. 1.8)
* numpy (1.6.1, 1.6.2)
* lxml (3.2.1)
* wget (2.0)


## Installing dependencies

With pip:

    sudo pip install biopython dendropy docutils lxml matplotlib networkx numpy wget

BioLite provides a large collection of wrappers for third party bioinformatics
tools. While you do not have to install these to be able to load the BioLite
python library or to use the BioLite command-line tools, a BioLite pipeline
that calls a wrapper must be able to find the corresponding program in your
PATH. Most of these can be installed using [Bioinformatics
Brew](http://bib.bitbucket.org), a cross-platform package manager for
bioinformatics tools.

If you are using a shared or research computing system, it is possible that
many of these packages are already available and you will not need to install
them. At runtime, BioLite will automatically attempt to find installed versions
of these packages using your PATH.


## Installing BioLite from the tarball

BioLite uses the standard `distutils` method of installation:

    tar xf biolite-0.4.0.tar.gz
    cd biolite-0.4.0
    python setup.py install

For an alternate install path, specify a prefix with:

    python setup.py install --prefix=...


## Installing BioLite from the git repo

Same as with the tarball, except clone the repo instead of downloading the tarball:

    git clone https://bitbucket.org/caseywdunn/biolite.git
    cd biolite
    python setup.py install

If you edit the code in the repository, or pull a new version, then run the following 
to update the installation:

    python setup.py install


## Generating the tarballs from the git repo

Create a source distribution with `distutils`:

    python setup.py sdist

Create a tarball for biolite-tools with GNU `autoconf`:

    cd tools
    ./autogen.sh
    ./configure
    make dist-gzip


## Building the reference manual

First, generate the Installation section of the manual from this INSTALL file using
[pandoc](http://johnmacfarlane.net/pandoc/installing.html):

    pandoc -f markdown -t rst INSTALL.md >doc/install.rst

Install sphinx and the Read the Docs sphinx theme:

    pip install sphinx sphinx_rtd_theme

Then run:

    cd doc
    make html

The reference manual is built in the `_build/html` subdirectory.


## Configuration

BioLite provides a default configuration file inside its python module at:

    (biolite.__path__) + 'config/biolite.cfg'

This file serves as the default configuration for any user on the system. To
override it on a per-user basis, simply copy the file to
`$HOME/.biolite/biolite.cfg` and make any required changes.

You can also override the location of the configuration file with an
environment variable. In `bash`:

    export BIOLITE_CONFIG=/your/path/to/biolite.cfg

or in `csh`:

    setenv BIOLITE_CONFIG /your/path/to/biolite.cfg

Finally, the BIOLITE_RESOURCE environment variable allows you to temporarily
override specific values in the resources section of the configuration. For
instance, if your configuration file is set to 2 threads, but want to test out
a run with 8 threads instead, you could use (in `bash`):

    export BIOLITE_RESOURCES="threads=8"

The value of this variable can be a comma-separated list of `key=value` pairs.


## Installing biolite-tools from the tarball

BioLite includes some C++ programs called biolite-tools. You can install them 
automatically with bib, or manually with the instructions provided here.

To compile biolite-tools, you must at a minimum have a C++ compiler that
supports the TR1 standard, e.g.:

* gcc 4.4 (CentOS 6.3)
* gcc 4.8 (Ubuntu 13.04)
* clang 5.1 (OS X 10.9)

biolite-tools uses the standard GNU `autoconf` method of installation:

    tar xf biolite-tools-0.4.0.tar.gz
    cd biolite-tools-0.4.0
    ./configure
    make install

For an alternate install path, specify a prefix with:

    ./configure --prefix=...
    make install


## Installing biolite-tools from the git repo

If you are installing biolite-tools directly from the git repo, you first need
to generate the configure script (which requires that you have `autoconf` and
`automake` installed):

    git clone https://bitbucket.org/caseywdunn/biolite.git
    cd biolite
    cd tools
    ./autogen.sh
    ./configure
    make install

