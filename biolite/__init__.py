# BioLite - Tools for processing gene sequence data and automating workflows
# Copyright (c) 2012-2014 Brown University. All rights reserved.
#
# This file is part of BioLite.
#
# BioLite is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# BioLite is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with BioLite.  If not, see <http://www.gnu.org/licenses/>.

"""
BioLite is a bioinformatics framework written in Python/C++ that automates the
collection and reporting of diagnostics, tracks provenance, and provides
lightweight tools for building out customized analysis pipelines.

Please cite:

Howison, M., Sinnott-Armstrong, N. A., & Dunn, C. W. (2012).  BioLite, a
lightweight bioinformatics framework with automated tracking of diagnostics and
provenance. In Proceedings of the 4th USENIX Workshop on the Theory and
Practice of Provenance (TaPP '12).
"""

__version__ = "0.5.0"
