Building pipelines
==================

.. _pipeline-module:

:mod:`pipeline` Module
----------------------

.. automodule:: biolite.pipeline
    :exclude-members: setup_fastq_paths
    :members:
    :undoc-members:
    :show-inheritance:

